import express from "express";
import type { Request, Response } from "express";

const app = express();

let count = 0;

app.get("/", (req: Request, res: Response) => {
	count++;
	res.json(count);
});

app.listen(3000, () => {
	console.log("Server is running on port 3000");
});
